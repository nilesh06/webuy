@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                	<h3>My Cart</h3>
                	@if(session('deleted'))
                		<div class="alert alert-success">
					        {{ session('deleted') }}
					    </div>
                	@endif
                	<table class="table">
                		<thead>
                			<tr>
                				<th>Sr No</th>
                				<th>Product Name</th>
                				<th>QTY</th>
                				<th>Price</th>
                				<th>Action</th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($all_carts as $key => $cart)
                				<tr>
                					<td>{{$key+1}}</td>
                					<td>{{$cart->product->name}}</td>
                					<td>{{$cart->qty}}</td>
                					<td>{{$cart->qty * $cart->product->price}}</td>
                					<td>
                						<a href="/deleteCart/{{$cart->id}}">
                							<button type="button" class="btn btn-danger deleteCart">X</button>
                						</a>
                					</td>
                				</tr>
                			@endforeach
                		</tbody>
                	</table>
	            </div>
            </div>
        </div>
    </div>
</div>
@endsection
