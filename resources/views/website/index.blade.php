@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                	<h3>All Products</h3>
                	@if(session('success'))
                		<div class="alert alert-success">
					        {{ session('success') }}
					    </div>
                	@endif
	                @foreach($products as $product)
	                	<div class="col-md-4">
	                		<img src="{{ asset('storage/images/'.$product->feature_image) }}" style="widows: 100px;height: 100px">
	                		<h5>{{$product->name}}</h5>
	                		<h6>{{$product->category->name}}</h5>
	                		<h6>${{$product->price}}</h5>
	                		<h6>{{$product->description}}</h5>
	                		<a href="/addToCart/{{$product->id}}">
	                			<button type="button" class="btn btn-info">Add To Cart</button>
	                		</a>
	                	</div>
	                @endforeach
	           	</div>
            </div>
        </div>
    </div>
</div>
@endsection
