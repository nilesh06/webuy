<header class="main-header">

    <a href="index2.html" class="logo">
        <span class="logo-mini"><b>PM</b></span>
        <span class="logo-lg"><b>WeBuy Test</b></span>
    </a>

    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    {{-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">12</span>
                    </a> --}}
                    <ul class="dropdown-menu">
                        <li class="header">You have 12 messages</li>
                        <li>
                            <ul class="menu">
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="{{asset('images/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                    </div>
                                    <h4>
										Support Team
										<small><i class="fa fa-clock-o"></i> 5 mins</small>
									</h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                        </li>
                        <!-- end message -->
                        <li>
                            <a href="#">
                                <div class="pull-left">
                                    <img src="{{asset('images/img/user3-128x128.jpg')}}" class="img-circle" alt="User Image">
                                </div>
                                <h4>
												AdminLTE Design Team
												<small><i class="fa fa-clock-o"></i> 2 hours</small>
											</h4>
                                <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="pull-left">
                                    <img src="{{asset('images/img/user4-128x128.jpg')}}" class="img-circle" alt="User Image">
                                </div>
                                <h4>
												Developers
												<small><i class="fa fa-clock-o"></i> Today</small>
											</h4>
                                <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="pull-left">
                                    <img src="{{asset('images/img/user3-128x128.jpg')}}" class="img-circle" alt="User Image">
                                </div>
                                <h4>
												Sales Department
												<small><i class="fa fa-clock-o"></i> Yesterday</small>
											</h4>
                                <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="pull-left">
                                    <img src="{{asset('images/img/user4-128x128.jpg')}}" class="img-circle" alt="User Image">
                                </div>
                                <h4>
												Reviewers
												<small><i class="fa fa-clock-o"></i> 2 days</small>
											</h4>
                                <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- <img src="{{asset('images/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"> -->
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <!-- <img src="{{asset('images/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image"> -->

                            <p>
                                {{Auth::user()->email}}
                                <!-- <small>Member since Jun. 2019</small> -->
                            </p>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>