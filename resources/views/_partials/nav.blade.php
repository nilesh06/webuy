<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="{{route('admin.home')}}">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
            </li>
            @php
                $category_array = array('admin/category/create','admin/category');
            @endphp
            <li class="treeview @if(in_array(Request::path(),$category_array)) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Category</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/category/create') ? 'active' : '' }}"><a href="{{ route('category.create') }}"><i class="fa fa-plus-circle"></i> Add</a></li>
                    <li class="{{ Request::is('admin/category') ? 'active' : '' }}"><a href="{{ route('category.index') }}"><i class="fa fa-list-ul"></i> View</a></li>
                </ul>
            </li>
            @php
                $product_array = array('admin/product/create','admin/product');
            @endphp
            <li class="treeview @if(in_array(Request::path(),$product_array)) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Product</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/product/create') ? 'active' : '' }}"><a href="{{ route('product.create') }}"><i class="fa fa-plus-circle"></i> Add Product</a></li>
                    <li class="{{ Request::is('admin/product') ? 'active' : '' }}"><a href="{{ route('product.index') }}"><i class="fa fa-list-ul"></i> View Products</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
