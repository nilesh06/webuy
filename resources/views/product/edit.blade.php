 @extends('layouts.masters')
 @section('title')
    Edit Product
 @endsection
 @section('page-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Product</h1>
            <ol class="breadcrumb">
                <li><a ><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Edit Product</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Product</h3>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('product.update',$product->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-8 col-xs-8">
                                        <div class="form-group">
                                            <label>NAME</label>
                                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ $product->name }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-8">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select id="selectbox" data-selected="" name="category_id" class="form-control">
                                                <option value="" selected="selected" disabled="disabled"> Select Category</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if($category->id == $product->category_id) selected @endif>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-8">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="number" class="form-control" id="price" placeholder="Price" name="price" value="{{ $product->price }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-8">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea name="description" placeholder="Description" class="form-control" style="resize: vertical;height: auto" rows="5" required>{{$product->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Feature Image</label>
                                                    <div class="upload-image">
                                                        <input type='file' class="imgInp" data-id='img3'  name="feature_image" onchange="document.getElementById('feature_image_show').src =window.URL.createObjectURL(this.files[0])" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="margin-top: 30px">
                                                    <img id="img3" src="{{ asset('storage/images/'.$product->feature_image) }}"  alt="" height="100" id="feature_image_show" style="width: 300px;height: 300px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="box-footer" align="center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>

    </div>
    <!-- //Content Wrapper -->
@endsection

@section('page-scripts')
    
@endsection
