 @extends('layouts.masters')
 @section('title')
 All Categories
 @endsection
  @section('page-content')
<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Categories</h1>
        <ol class="breadcrumb">
            <li><a ><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Categories</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
              @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Categories List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped" style="width: 100%">
                                    <thead>
                                        <tr role="row" style="text-align: center;">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">#</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 142px;">Category Name</th>

                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 161px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $key => $category)

                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{++$key}}</td>
                                                <td class="sorting_1">{{$category->name}}</td>
                                                <td style="display:flex;">
                                                    <a href="{{route('category.edit',['id'=>$category->id])}}" style="margin-right: 15px;margin-top:2px;">
                                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                                    </a>
                                                    <form method="POST" action="{{route('category.destroy',$category->id)}}">
                                                      {{csrf_field()}}
                                                      {{method_field('DELETE')}}
                                                      <button type="submit" data-toggle="tooltip" data-placement="top" title="Delete" style="background:transparent;border:none;"><i class="fa fa-trash" aria-hidden="true" style="color:red;"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
