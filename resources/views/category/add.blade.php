 @extends('layouts.masters')
 @section('title')
 Add Category
 @endsection
 @section('page-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Category</h1>
            <ol class="breadcrumb">
                <li><a ><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Add Category</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Category</h3>
                        </div>
                        @if ($errors->any())
                         <div class="alert alert-danger">
                            <ul>
                               @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                               @endforeach
                            </ul>
                         </div>
                         @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-5 col-sm-8 col-xs-8">
                                        <div class="form-group">
                                            <label>CATEGORY NAME</label>
                                            <input type="text" class="form-control" id="name" placeholder="Enter Category Name" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>
    <!-- //Content Wrapper -->
@endsection
@section('page-scripts')
@endsection
