<!DOCTYPE html>
<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
		@include('_partials.link')
		@yield('page_links')

        @yield('page-style')
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		@php
			date_default_timezone_set('Asia/Kolkata');
		@endphp
		<div class="wrapper">


    		@include('_partials.top')
    		@include('_partials.nav')

     	@yield('page-content')
    </div>
	    @include('_partials.footer')
		@include('_partials.scripts')
		@yield('page-scripts')

	</body>
</html>
