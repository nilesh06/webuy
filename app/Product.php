<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = "products";

    protected $fillable = [
    	'category_id',
    	'name',
    	'description',
    	'feature_image',
    	'price',
    ];

    public function category(){
      return $this->belongsTo(Category::class);
    }
}
