<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','DESC')->get();
        return view('product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('product.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
              'name'=>'required',
              'category_id'=>'required',
              'description'=>'required',
              'price'=>'required',
              'feature_image'=>'required',
            ],
            [
                "category_id.required"=>"Please Select Category",
                "feature_image.required"=>"Please Upload Feature Image",
            ]
        );

        $request_array = array();
        $request_array = $request->all();

        $featured_imageName = '';
        if($request->feature_image != null)
        {
            $imageName = 'feature_image_'.$request->feature_image->getClientOriginalName();

            $request->file('feature_image')->move(base_path() . '/public/storage/images/', $imageName);

            $featured_imageName = $imageName;

            $request_array['feature_image'] = $imageName;
        }
        else{
          $request_array['feature_image'] = '';
        }
        Product::create($request_array);

        return redirect()->route('product.index')->with('message','Product added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('product.edit',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request,
            [
              'name'=>'required',
              'category_id'=>'required',
              'description'=>'required',
              'price'=>'required',
            ],
            [
                "category_id.required"=>"Please Select Category",
            ]
        );

        $request_array = array();
        $request_array = $request->all();   

        $featured_imageName = '';
        if($request->feature_image != null)
        {
            $imageName = 'feature_image_'.$request->feature_image->getClientOriginalName();

            $request->file('feature_image')->move(base_path() . '/public/storage/images/', $imageName);

            $featured_imageName = $imageName;

            $request_array['feature_image'] = $imageName;
        }
        else{
          $request_array['feature_image'] = $product->feature_image;
        }
        $product->update($request_array);

        return redirect()->route('product.index')->with('message','Product updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
