<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Auth;
use App\Cart;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function website()
    {
        $products = Product::all();

        return view('website.index',compact('products'));
    }

    public function addToCart($product_id)
    {
        // dd(Auth::user()->id);
        $check = Cart::where('user_id',Auth::user()->id)->where('product_id',$product_id)->first();

        if(isset($check))
        {
            Cart::where('user_id',Auth::user()->id)->where('product_id',$product_id)->update([
                'qty'=>$check->qty + 1,
            ]);
        }
        else
        {
            Cart::create([
                'user_id'=>Auth::user()->id,
                'product_id'=>$product_id,
                'qty'=>1,
            ]);
        }

        return redirect()->back()->with('success','Added in cart successfully');
    }

    public function myCart()
    {
        $all_carts = Cart::where('user_id',Auth::user()->id)->get();

        return view('website.mycart',compact('all_carts'));
    }

    public function deleteCart($id)
    {
        Cart::where('id',$id)->delete();

        return redirect()->back()->with('deleted','Removed From Cart Successfully');
    }
}
