<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/website', 'HomeController@website')->name('website');
Route::get('/addToCart/{product_id}', 'HomeController@addToCart')->name('addToCart');
Route::get('/myCart', 'HomeController@myCart')->name('myCart');
Route::get('/deleteCart/{id}', 'HomeController@deleteCart');

Route::group(['prefix'=>'admin','middleware'=>['auth']],function(){
	Route::redirect('/','login');
	Route::get('/home', 'HomeController@index')->name('admin.home');
	Route::resource('category','CategoryController');
  	Route::resource('product','ProductController');
});
